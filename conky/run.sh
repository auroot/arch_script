#/bin/bash
#
#
clear;
echo "#============================================="
echo "# Name:           Arch Linux程序启动"
echo "# Version:        v0.1"
echo "# Author:         Basierl QQ2763833502"
echo "# Copyright:	  https://www.basierl.com/"
echo "#============================================="
echo
echo -e "\033[1;36m   1.开启Conky \033[0m"
echo -e "\033[1;36m   2.关闭Conky \033[0m"
echo -e "\033[1;36m   3.挂载VirtualDisk目录 \033[0m"
echo -e "\033[1;36m   4.卸载VirtualDisk目录 \033[0m"
echo -e "\033[1;36m   5.开启SSR代理 \033[0m"
#echo -e "\033[1;36m   6.开启Conky \033[0m"
#echo -e "\033[1;36m   7.开启Conky \033[0m"
#echo -e "\033[1;36m   8.开启Conky \033[0m"
#echo -e "\033[1;36m   9.开启Conky \033[0m"
echo -e "\033[1;36m   0.全部执行 \033[0m"
echo -e "\033[1;36m   n.开启网易云音乐 \033[0m"
echo -e "\033[1;36m  up.滚动更新 \033[0m"
echo -e "\033[1;36m   q. 退出 \033[0m"
echo
read -p 	"要我帮你干嘛呀! [1-9/a-z]?: " plugin
#
#--------------1 号
	if [[ $plugin == 1 ]];then
	echo && echo -e "\033[1;5;37m  正在开启Conky \033[0m"
	echo 
	sleep 2
	cd /home/auangz/.Conky 
	bash startconky.sh
	clear;
	echo -e "\033[1;5;32m  ${Info} Conky开启完成! \033[0m"
	exit;0
	fi
#--------------2 号
	if [[ $plugin == 2 ]];then
	clear;
	echo && echo -e "\033[1;5;37m  正在关闭Conky \033[0m"
	echo 
	sleep 2
#	top -b -d 1 -n 1 | grep 'conky' | cut -f1 -d" " >> /tmp/kills
	ps -ef | grep conky | grep -v 'grep' | awk '{print $2}' >> /tmp/kills
	echo -e "\033[1;5;37m杀死以下进程 PID ↓ \033[0m \n`cat /tmp/kills`" 
#	kill `ps | grep "conky" | cut -d" " -f1` > /dev/null
	kill `ps -ef | grep conky | grep -v 'grep' | awk '{print $2}'` > /dev/null
	echo "orange" | sudo -S rm -rf /tmp/kills
	echo -e "\033[1;5;32m  ${Info} 成功了Conky关闭! \033[0m"
	sleep 3
	clear;
	exit;0
	fi
#--------------3 号
	if [[ $plugin == 3 ]];then
	echo && echo -e "\033[1;5;37m  正在挂载VirtualDisk目录 \033[0m"
	echo 
	sudo mkdir -p /mnt/Auangz/virtualdisk
	sleep 3
	echo "orange" | sudo -S mount -U EC22C55022C52086 /mnt/Auangz/virtualdisk
	clear
	lsblk /dev/sdc
	echo -e "\033[1;5;32m  ${Info} VirtualDisk挂载完成! \033[0m"
	exit;0
	fi
#--------------4 号
	if [[ $plugin == 4 ]];then
	echo && echo  -e "\033[1;5;37m  正在卸载VirtualDisk目录 \033[0m"
	echo 
	sleep 3
	echo "orange" | sudo -S umount -R  /mnt/Auangz/virtualdisk
	clear
	lsblk /dev/sdc
	echo -e "\033[1;5;32m  ${Info} VirtualDisk卸载完成! \033[0m"
	exit;0
	fi
#--------------5 号
	if [[ $plugin == 5 ]];then
	echo && echo -e "\033[1;5;37m  正在为您开启SSR代理 \033[0m"
	echo 
	sleep 2
	cd /home/auangz/Documents
	./electron-ssr-0.2.6.AppImage >/dev/null
	echo -e "$\033[1;5;32m  {Info} SSR代理以开启! \033[0m"
	exit;0
	fi
#

#=================================字母别名====================================#
#--------------n N
	if [[ $plugin == 11  ]];then
	echo && echo -e "\033[1;5;37m  正在为您开启网易云音乐 \033[0m"
	echo 
	sleep 1
	netease-cloud-music;
	echo -e "$\033[1;5;32m  {Info} 网易云音乐以开启! \033[0m"
	clear;
	exit;0
	fi
#--------------upgrade
	if [[ $plugin == up ]];then
	clear;
	echo
	read -n1 -t5 -p "  可能会使系统崩溃，请谨慎滚动更新 [Y/N]? : " plugin1
		case $plugin1 in
			Y | y)
				sleep 2
				echo "orange" | sudo -S pacman -Syyu
				echo
				echo -e "\033[1;36m${Info}以执行滚动更新，坐等灭亡吧! \033[0m"
			;;
			N | n)
				sleep 2
				echo
				echo -e "\033[1;32m  哈哈哈，我就知道你不敢！！！ \033[0m"
			;;
			* )
				echo 
				echo
    				echo -e "\033[1;38mError choice:\033[33m $plugin1.\033[0m"
				exit;0
			;;
		esac
	fi
#=================================999====================================#
#
	if [[ $plugin == 9 ]];then
	sleep 2
	echo && echo -e "\033[1;5;37m  正在执行....\033[0m"
	echo 
	cd /home/auangz/.Conky
	bash startconky.sh
	echo -e "\033[1;5;32m  ${Info} Conky开启完成! \033[0m"
	sleep 2
	echo "orange" | sudo -S mount /dev/sdc1 /mnt/Auangz/virtualdisk
	echo -e "\033[1;5;32m  ${Info} VirtualDisk挂载完成! \033[0m"
	exit;0
	fi
#=================================quit====================================#
	if [[ $plugin == q || Q || quit ]];then
	clear;
	echo
	echo -e "\033[1;5;32m  ${Info} 感谢您的使用! \033[0m"
	exit;0
	fi

