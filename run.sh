#!/bin/bash
#
basierl_arch=`pwd` 	 #---输入脚本父目录
arundir="`pwd`/conky"   	 #---输入conky父目录
pwd
null="/dev/null"
# 颜色
r='\033[1;31m'	#---红
g='\033[1;32m'	#---绿
y='\033[1;33m'	#---黄
b='\033[1;36m'	#---蓝
w='\033[1;37m'	#---白
rw='\033[1;41m'    #--红白
wg='\033[1;42m'    #--白绿
ws='\033[1;43m'    #--白褐
wb='\033[1;44m'    #--白蓝
wq='\033[1;45m'    #--白紫
wa='\033[1;46m'    #--白青
wh='\033[1;46m'    #--白灰
h='\033[0m'		 #---后缀
bx='\033[1;4;36m' #---蓝 下划线
#
clear;
ECHOA=`echo -e "${w}    _             _       _     _                  ${h}"`  
ECHOB=`echo -e "${g}   / \   _ __ ___| |__   | |   (_)_ __  _   ___  _        ${h}"` 
ECHOC=`echo -e "${b}  / _ \ | '__/ __| '_ \  | |   | | '_ \| | | \ \/ /         ${h}"` 
ECHOD=`echo -e "${y} / ___ \| | | (__| | | | | |___| | | | | |_| |>  <           ${h}"`  
ECHOE=`echo -e "${r}/_/   \_\_|  \___|_| |_| |_____|_|_| |_|\__,_/_/\_\                ${h}"`
#echo -e "$ECHOA\n$ECHOB\n$ECHOC\n$ECHOD\n$ECHOE" | lolcat 2> ${null} || echo -e "$ECHOA\n$ECHOB\n$ECHOC\n$ECHOD\n$ECHOE"
echo -e "$ECHOA\n$ECHOB\n$ECHOC\n$ECHOD\n$ECHOE" || echo -e "$ECHOA\n$ECHOB\n$ECHOC\n$ECHOD\n$ECHOE"
echo -e "#=======================================================#"
echo -e "# ${g}Name:           Arch Linux Custom script${h}"
echo -e "# ${g}Version:        V1.5${h}"
echo -e "# ${g}Author:         Basierl QQ2763833502${h}"
echo -e "# ${g}Copyright:	  ${wd}https://github.com/BaSierL/arch_install${h}"
echo -e "#=======================================================#"
echo
echo -e "${w}:: ==>> Open Conky 		[1]${h}"
echo -e "${w}:: ==>> Mount Disk		[2]${h}"
echo -e "${w}:: ==>> Configure SynPS/2 	[3]${h}"
echo -e "${w}:: ==>> Open VMware		[4]${h}"
#echo -e "${w}:: ==>> 开启Conky [5]${h}"
#echo -e "${w}:: ==>> 开启Conky [6]${h}"
#echo -e "${w}:: ==>> 全部执行 [7]${h}"
echo -e "${w}:: ==>> Install System 		[in]${h}"
echo -e "${w}:: ==>> Cleaning system 	[pr]${h}"
echo -e "${w}:: ==>> Scroll Update 		[up]${h}"
echo -e "${w}:: ==>> quit 			[quit]${h}"
echo
echo -e "${y}:: ==>> What are the tasks[1,2,3..]? ${h}"
read -p ":: ==>> : " instructions
#echo "${UPASS:- }"
# echo "${PASSWORD:+YOU}"
#
#
#
#
#--------------1 号
if [[ $instructions == 1 ]];then
echo;
read  -p ":: 开启Conky[Y] 关闭Conky[N] 返回[00]? :" conkys
read -p ":: ==>> User Password [Root Permission Skip]: " UPASS
case $conkys in
	Y | y)
		sleep 1
		echo && echo -e "$w:: 正在开启Conky $h"
		echo 
		sleep 2 
		bash $arundir/startconky.sh &> /dev/null || echo -e "$g:: Conky开启完成! $h" && echo -e "$r:: Conky开启失败! $h"
		exit 0
;;
	N | n)
		echo;
		echo && echo -e "$w:: 正在关闭Conky $h" 
		sleep 1
		ps -ef | grep conky | grep -v 'grep' | awk '{print $2}' >> /tmp/kills
		echo -e "$w:: 杀死以下进程 PID ↓ $h \n`cat /tmp/kills`" 
		kill `ps -ef | grep conky | grep -v 'grep' | awk '{print $2}'` > /dev/null
		echo "${UPASS}" | sudo -S rm -rf /tmp/kills
		echo -e "$g:: 成功了Conky关闭! $h"
		sleep 1
		exit 0
;;
	00 )
		echo 
			sh ${basierl_arch}/run.sh
;;
	* )
		echo 
			echo -e ":: 错误指令: ${w}$conky.$h"
		exit 1
;;
esac
fi
#--------------2 号
if [[ $instructions == 2 ]];then
#
WINDOWS_S="/dev/nvme0n1p2"	#磁盘设备名称
WINDOWS_D="/mnt/windows"	#目录地址
#
Linux_S="/dev/sda1"		#磁盘设备名称
Linux_D="/mnt/Linux"  	 #目录地址
#
D_disk_S="/dev/sda3"		#磁盘设备名称
D_disk="/mnt/D_disk"		#目录地址
#
echo;
read  -p ":: 挂载[Y] 卸载[N] 返回[00]? :" virtuald
read -p ":: ==>> User Password [Root Permission Skip] : " DISKUPASS
case $virtuald in
	Y | y)
#		sh ${basierl_arch}/mount_disk/mount.sh
#		clear;
		echo
		sleep 1
		echo "${DISKUPASS}" | sudo -S mkdir ${WINDOWS_D} ${Linux_D} ${D_disk} &> /dev/null
		echo "${DISKUPASS}" | sudo -S mount ${WINDOWS_S} ${WINDOWS_D} &> /dev/null || echo ":: Error: ${WINDOWS_D}."
		echo "${DISKUPASS}" | sudo -S mount ${Linux_S} ${Linux_D} &> /dev/null || echo ":: Error: ${Linux_D}."
		echo "${DISKUPASS}" | sudo -S mount ${D_disk_S} ${D_disk} &> /dev/null || echo ":: Error: ${D_disk}." && exit 22
		echo
		lsblk

		exit 0
;;
	N | n)
		echo;
		sleep 1
		echo "${DISKUPASS}" | sudo -S umount -R ${WINDOWS_D} &> /dev/null && echo "Success Umount: ${WINDOWS_D}"
		echo "${DISKUPASS}" | sudo -S umount -R ${Linux_D} &> /dev/null && echo "Success Umount: ${Linux_D}"
		echo "${DISKUPASS}" | sudo -S umount -R ${D_disk} &> /dev/null && echo "Success Umount: ${D_disk}" || exit 23
		echo;
		lsblk
		exit 0
;;
	00 )
		echo 
		sh ${basierl_arch}/run.sh
;;
	   * )
		echo 
			echo -e ":: 错误指令: ${w}$virtuald.$h"
		exit 2
;;
esac
fi
#--------------3 号
XYN="`xinput list | grep "SynPS/2" | grep -o "[0-9][0-9]"`"
if [[ $instructions == 3 ]];then
	echo
	echo ":: 检测到您系统的SynPS/2 ID为: $XYN."
	echo
	read  -p ":: 开启触控板[Y] 关闭触控板[N] 返回[00]? :"  board
case $board in
	Y | y)
		sleep 1
		echo
		xinput enable $XYN && echo -e "$y:: 已经开启SynPS/2... $h"
		echo
;;
	N | n)
		sleep 1
		echo
		xinput disable $XYN && echo -e "$y:: 已经关闭SynPS/2... $h"
		echo
;;
	00 )
		echo 
		sh ${basierl_arch}/run.sh
;;
	   * )
		echo 
		echo -e ":: 错误指令: ${w}$board.$h"
		exit;3
;;
esac
fi

#--------------4 号
if [[ $instructions == 4 ]];then
#====================== 变量
VUSBS="vmware-usbarbitrator.service"
VNETS="vmware-networks.service"
VHS="vmware-hostd.service"
VAS="vmware-authd.service"
VVFS="vmware-vmblock-fuse.service"
VHCS="vmware-hostd-certificates.service"
#======================
read -p ":: ==>> User Password [Root Permission Skip]: " UPASS
read  -p ":: 开启VMware服务[Y] 关闭VMware服务[N] 返回[00]? :" VMWARE_SERVICE
case $VMWARE_SERVICE in
	Y | y)
		sleep 1
		echo
		echo -e "$y:: 正在开启 VMware服务... $h"
		echo "$UPASS" | sudo -S systemctl start $VUSBS && echo -e "$g:: Running: $VUSBS. $h" ||  echo -e "$r:: Error: $VUSBS. $h" \
			sudo -S systemctl start $VNETS && echo -e "$g:: Running: $VNETS. $h" ||  echo -e "$r:: Error: $VNETS. $h" \
			sudo -S systemctl start $VHS && echo -e "$g:: Running: $VHS. $h" ||  echo -e "$r:: Error: $VHS. $h" \
			sudo -S systemctl start $VAS && echo -e "$g:: Running: $VAS. $h" ||  echo -e "$r:: Error: $VAS" \ 
			sudo -S systemctl start $VVFS && echo -e "$g:: Running: $VVFS. $h" ||  echo -e "$r:: Error: $VVFS. $h" \
			sudo -S systemctl start $VHCS && echo -e "$g:: Running: $VHCS. $h" ||  echo -e "$r:: Error: $VHCS. $h"
			echo;
;;
	N | n)
		sleep 1
		echo
		echo -e "$y:: 正在关闭 VMware服务... $h"
		echo "${UPASS}" | sudo -S systemctl stop $VUSBS &> ${null} && echo -e "$g:: Success: $VUSBS. $h"  ||  echo -e "$r:: Error: $VUSBS. $h"\
			sudo -S systemctl stop $VNETS && echo -e "$g:: Success: $VNETS. $h" ||  echo -e "$r:: Error: $VNETS. $h" \
			sudo -S systemctl stop $VHS && echo -e "$g:: Success: $VHS. $h" ||  echo -e "$r:: Error: $VHS. $h" \
			sudo -S systemctl stop $VAS && echo -e "$g:: Success: $VAS. $h" ||  echo -e "$r:: Error: $VAS. $h" \
			sudo -S systemctl stop $VVFS && echo -e "$g:: Success: $VVFS. $h" ||  echo -e "$r:: Error: $VVFS. $h" \
			sudo -S systemctl stop $VHCS && echo -e "$g:: Success: $VHCS. $h" ||  echo -e "$r:: Error: $VHCS. $h"
;;
	00 )
		echo 
		sh ${basierl_arch}/run.sh
;;
	   * )
		echo 
		echo -e ":: 错误指令: ${w}$VMWARE_SERVICE.$h"
		exit 4
;;
esac
fi
#--------------5 号
#
#=================================字母别名====================================#
#--------------upgrade
if [[ $instructions == up ]];then
	echo
	read -p ":: 可能会使系统崩溃，请谨慎滚动更新 [Y/N] 返回[00]? : " plugin1
	read -p ":: ==>> User Password [Root Permission Skip]: " UPASS
		case $plugin1 in
			Y | y)
				sleep 2
				echo "$PASSWORD" | sudo -S pacman -Syyu
				echo
				echo -e "$b:: 以执行滚动更新，坐等灭亡吧! $h"
			;;
			N | n)
				sleep 2
				echo
				echo -e "$g:: 哈哈哈，我就知道你不敢！！！ $h"
			;;
			00 )
				echo 
				sh ${basierl_arch}/run.sh
			;;
			* ) 
				echo
				echo -e ":: 错误指令: ${w}${plugin1}.$h"
				exit 100
			;;
		esac
	fi
#--------------pr
if [[ $instructions == pr  ]];then
read -p ":: ==>> User Password [Root Permission Skip]: " UPASS
	echo && echo -e "$w:: 正在为您清除系统中无用的包: $h"
	echo
	echo "$UPASS" | sudo -S pacman -R $(pacman -Qdtq) \
		sudo -S pacman -R $(pacman -Qdtq) \
		sudo -S pacman -R $(pacman -Qdtq) \
		sudo -S pacman -R $(pacman -Qdtq) \
		sudo -S pacman -R $(pacman -Qdtq)
	echo 
	echo && echo -e "$w:: 正在为您清除已下载的安装包: $h"
	echo "$UPASS" | sudo pacman -Scc
	sleep 1
	clear;
	echo
	echo -e "$g:: 垃圾清理完毕! $h"
	exit 101
fi

#-------------- in  
if [[ $instructions == in  ]];then
	echo;
	echo -e "${g}:: ==>> Installation script language: Chinese[CN] English[EN] Return[00]? ${h}"
	read -p ":: ==>> : " IN_ARCH
		case $IN_ARCH in
			CN | cn | Cn)
				sleep 2
				sh ${basierl_arch}/arch_install/arch_install.sh
				clear;
			;;
			EN | en | En)
				sleep 2
				sh ${basierl_arch}/arch_install/arch_install_en.sh
				clear;
			;;
			00 )
				echo 
				sh ${basierl_arch}/run.sh
			;;
			* )
				echo 
				echo -e ":: 错误指令: ${w}${IN_ARCH}.$h"
				exit 1
			;;
		esac
	fi
#==========================================================================#

#=================================quit====================================#
if [[ $instructions == q || Q || quit || QUIT ]];then
	#clear;
	exit 127
fi




