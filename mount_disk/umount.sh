#!/bin/bash

# 挂载目录 变量  需要的自己加
#basierl_arch="../"

# WINDOWS_S="/dev/nvme0n1p2"	#磁盘设备名称
WINDOWS_D="/mnt/windows"	#目录地址

# Linux_S="/dev/sda1"		#磁盘设备名称
Linux_D="/mnt/Linux"  	 #目录地址

# D_disk_S="/dev/sda3"		#磁盘设备名称
D_disk="/mnt/D_disk"		#目录地址

read -p ":: ==>> Open[N/n] :" DISKXZ
read -p ":: ==>> User Password [Root Permission Skip] : " DISKUPASS
case $DISKXZ in
	N | n)
		echo;
		echo "${DISKUPASS}" | sudo -S umount -R ${WINDOWS_D} &> /dev/null && echo "Success Umount: ${WINDOWS_D}"
		echo "${DISKUPASS}" | sudo -S umount -R ${Linux_D} &> /dev/null && echo "Success Umount: ${Linux_D}"
		echo "${DISKUPASS}" | sudo -S umount -R ${D_disk} &> /dev/null && echo "Success Umount: ${D_disk}"
		echo;
		lsblk
		exit 0
;;       
    * ) 
		echo
		echo -e ":: 错误指令: ${w}${DISKXZ}.$h"
		exit 102
;;
esac

